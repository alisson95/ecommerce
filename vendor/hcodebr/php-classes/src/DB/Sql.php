<?php 

namespace Hcode\DB;

class Sql {

	const HOSTNAME = "127.0.0.1";
	const USERNAME = "root";
	const PASSWORD = "1234";
	const DBNAME = "db_ecommerce";

	private $conn;

	public function __construct()
	{

		$this->conn = new \PDO(
			"mysql:dbname=".Sql::DBNAME.";host=".Sql::HOSTNAME, 
			Sql::USERNAME,
			Sql::PASSWORD
		);

	}

	private function setParams($statement, $parameters = array())
	{

		foreach ($parameters as $key => $value) {
			
			$this->bindParam($statement, $key, $value);

		}

	}

	private function bindParam($statement, $key, $value)
	{

		$statement->bindParam($key, $value);

	}

	public function query($rawQuery, $params = array())
	{

		$stmt = $this->conn->prepare($rawQuery);

		$this->setParams($stmt, $params);

		$stmt->execute();

	}

	public function select($rawQuery, $params = array()):array
	{

		$stmt = $this->conn->prepare($rawQuery);
	
		$this->setParams($stmt, $params);


						$file = fopen("log.txt","a+");

						$teste = "\r\n" . $this->showQuery($rawQuery, $params);

						//"\r\n" pular linha
						fwrite($file,$teste);

						fclose($file);

		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);

	}


	private function showQuery($query, $params)
    {
        $keys = array();
        $values = array();

        # build a regular expression for each parameter
        foreach ($params as $key=>$value)
        {

            if(is_numeric($value))
            {
                $valueReplace = intval($value);
            }
            else
            {
                $valueReplace = '"'. $value .'"';
            }

            $query = str_replace($key, $valueReplace, $query);
        }
        
        return $query;
    }

	public function update()
	{
		$sql = new Sql();

		$params =  array(
			":iduser"=>$this->getiduser(),
			":desperson"=>$this->getdesperson(),
			":deslogin"=>$this->getdeslogin(),
			":despassword"=>$this->getdespassword(),
			":desemail"=>$this->getdesemail(),
			":nrphone"=>$this->getnrphone(),
			":inadmin"=>$this->getinadmin());

		$results = $sql->select("CALL sp_usersupdate_save(:iduser, :desperson, :deslogin, :despassword, :desemail, :nrphone, :inadmin)",$params);


		$this->setData($results[0]);

		//a alteração não está funcionando 
	}


}

 ?>