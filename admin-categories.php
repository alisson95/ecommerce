<?php

use \Hcode\PageAdmin;
use \Hcode\Model\User;
use \Hcode\Model\Category;
use \Hcode\Model\Product;


$app->get('/admin/categories', function() {

	User::verifyLogin();

	$categories = Category::listAll();

	$page = new PageAdmin();

	$page->setTpl("categories", array(
		"categories"=>$categories
	));

});


$app->get('/admin/categories/create', function() {

	User::verifyLogin();

	$page = new PageAdmin();

	$page->setTpl("categories-create");

});

$app->post('/admin/categories/create', function() {

	User::verifyLogin();

	$category = new Category();

	$category->setData($_POST);

	$category->save();

	header("Location: /admin/categories");

	exit;
});

$app->get('/admin/categories/:idcatogory/delete', function($idcatogory) {

	User::verifyLogin();

	$category = new Category();

	$category->get((int)$idcatogory);

	$category->delete();

	header("Location: /admin/categories");

	exit;
});

$app->get('/admin/categories/:idcatogory', function($idcatogory) {

	User::verifyLogin();

	$category = new Category();

	$category->get((int)$idcatogory);
	
	$page = new PageAdmin();

	$page->setTpl("categories-update", array(
		"category"=>$category->getValues()
	));

});

$app->post('/admin/categories/:idcatogory', function($idcatogory) {

	User::verifyLogin();

	$category = new Category();

	$category->get((int)$idcatogory);
	
	$category->setData($_POST);

	$category->save();

	header("Location: /admin/categories");

	exit;
});

$app->get('/admin/categories/:idcatogory/products', function($idcatogory) {

	User::verifyLogin();

	$category = new Category();
	
	$category->get((int)$idcatogory);
	
   	$page = new PageAdmin();

   	$page->setTpl("categories-products", array(
   		'category'=>$category->getValues(),
   		'productsRelated'=>$category->getProducts(true),
   		'productsNotRelated'=>$category->getProducts(false)   		
    ));
});

$app->get('/admin/categories/:idcatogory/products/:idproduct/add', function($idcatogory, $idproduct) {

	User::verifyLogin();

	$category = new Category();
	$category->get((int)$idcatogory);

	$product = new Product();
	$product->get((int)$idproduct);

	$category->addProduct($product);

	header("Location: /admin/categories/" . $idcatogory . "/products");
	exit;
});


$app->get('/admin/categories/:idcatogory/products/:idproduct/remove', function($idcatogory, $idproduct) {

	User::verifyLogin();

	$category = new Category();
	$category->get((int)$idcatogory);

	$product = new Product();
	$product->get((int)$idproduct);

	$category->removeProduct($product);

	header("Location: /admin/categories/" . $idcatogory . "/products");
	exit;
});


?>